import multiprocessing
import time

import pika

WORKERS = 5


def callback(ch, method, properties, body):
    print(f"{multiprocessing.current_process()} {body}")
    time.sleep(5)


def consume():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='hello')
    channel.basic_consume(on_message_callback=callback,
                          queue='hello', auto_ack=True)

    print(' [*] Ожидание сообщений. CTRL+C для выхода')
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        pass

pool = multiprocessing.Pool(processes=WORKERS)
for i in range(0, WORKERS):
    pool.apply_async(consume)

try:
    while True:
        continue
except KeyboardInterrupt:
    print(' [*] Выход...')
    pool.terminate()
    pool.join()


