import multiprocessing
import time

import pika

ARRAY = [1, 2, 3, 4, 5, 6, 7, 8, 9]
WORKERS = 6


def output(data):
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='hello')
    channel.basic_publish(exchange='',
                          routing_key='hello',
                          body=str(data))
    print(f"{multiprocessing.current_process()} {data}")
    time.sleep(1)



with multiprocessing.Pool(WORKERS) as pool:
    pool.map(output, ARRAY)


